angular.module('myApp.controllers', ['ui.router'])
    .controller('MainController', function($scope) {
        $scope.title = 'Hello, Cordova!';
        $scope.message = 'This is an AngularJS app running in a Cordova environment.';
    });
