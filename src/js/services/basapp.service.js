'use strict'

angular
  .module('myApp')
  .service('BasApp', [
    '$window',
    BasApp
  ])

/**
 * @typedef {Object} TBasAppState
 * @property {string} version
 * @property {string} versionFull
 * @property {boolean} isPaused
 * @property {string} networkConnectionType
 */

/**
 * Helper service for several App functions
 *
 * @constructor
 * @param $window
 */
function BasApp (
  $window
) {

  /**
   * @type {TBasAppState}
   */
  var basAppState = {}
  basAppState.isPaused = false
  basAppState.networkConnectionType = 'none'

  this.get = get
  this.isPaused = isPaused

  init()

  function init () {

    _setAppListeners()
  }

  /**
   * @returns {TBasAppState}
   */
  function get () {
    return basAppState
  }

  /**
   * @returns {boolean}
   */
  function isPaused () {
    return basAppState.isPaused
  }

  /**
   * Set all app listeners (cordova and others)
   *
   * @private
   */
  function _setAppListeners () {
    console.log('setting app listeners')
    // Cordova events
    $window.document.addEventListener(
      'resume',
      _onResume,
      false
    )
    $window.document.addEventListener(
      'pause',
      _onPause,
      false
    )
  }

  /**
   * Cordova event - Resume
   */
  function _onResume () {

    console.log('App - Cordova RESUME')

    // Reset has paused
    basAppState.isPaused = false
  }

  /**
   * Cordova event - Pause
   */
  function _onPause () {

    // Set paused state
    basAppState.isPaused = true
    $state.go('hello')
    console.log('App - Cordova PAUSE')
  }
}
