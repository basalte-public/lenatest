
import angular from 'angular';
import uiRouter from '@uirouter/angularjs';

const requires = [
    'ui.router',
];


// Mount on window for testing
window.app = angular.module('myApp', requires);
angular.module('myApp').config(
    function($stateProvider, $urlRouterProvider) {
    var helloState = {
        name: 'hello',
        url: '/hello',
        template: '<h3>hello world!</h3>'
    }

    var aboutState = {
        name: 'about',
        url: '/about',
        template: '<h3>Its the UI-Router hello world app!</h3>'
    }

    $stateProvider.state(helloState);
    $stateProvider.state(aboutState);
})

angular.module('myApp').service('BasApp', [
    '$window',
    '$state',
    BasApp
])

/**
 * @typedef {Object} TBasAppState
 * @property {string} version
 * @property {string} versionFull
 * @property {boolean} isPaused
 * @property {string} networkConnectionType
 */

/**
 * Helper service for several App functions
 *
 * @constructor
 * @param $window
 * @param $state
 */
function BasApp (
    $window,
    $state
) {

    /**
     * @type {TBasAppState}
     */
    var basAppState = {}
    basAppState.isPaused = false
    basAppState.networkConnectionType = 'none'

    this.get = get
    this.isPaused = isPaused

    init()

    function init () {

        _setAppListeners()
    }

    /**
     * @returns {TBasAppState}
     */
    function get () {
        return basAppState
    }

    /**
     * @returns {boolean}
     */
    function isPaused () {
        return basAppState.isPaused
    }

    /**
     * Set all app listeners (cordova and others)
     *
     * @private
     */
    function _setAppListeners () {
        console.log('setting app listeners')
        // Cordova events
        // device APIs are available
        function onDeviceReady() {
            console.log('onDeviceReady')
            document.addEventListener("resume", _onResume, false);
            document.addEventListener("pause", _onPause, false);
        }

        document.addEventListener("deviceready", onDeviceReady, false);
    }

    /**
     * Cordova event - Resume
     */
    function _onResume () {

        console.log('App - Cordova RESUME')

        // Reset has paused
        basAppState.isPaused = false
    }

    /**
     * Cordova event - Pause
     */
    function _onPause () {

        // Set paused state
        basAppState.isPaused = true
        $state.go('hello')
        console.log('App - Cordova PAUSE')
    }

    function _onVisibilityChange () {
        console.log('App - Cordova _onVisibilityChange')
    }
}

// angular.module('myApp', [])
//     .controller('MainController', function($scope) {
//         $scope.onResumeFromNative = function() {
//             console.log("App resumed from native code");
//             // Your custom logic here
//         };
//
//         document.addEventListener('deviceready', function() {
//             // Any other initialization code
//         }, false);
//     });

angular.module('myApp').run(['BasApp', function(BasApp){
    console.log('running app..')
}])


